#include <stdbool.h>
#include <stddef.h>
#include <string.h>

#define PTR_DIFF(p1,p2) ((ptrdiff_t)(((const char *)(p1)) - (const char *)(p2)))
